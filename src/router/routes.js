import Home from "@/components/pages/Home";
import User from "@/components/pages/User";
import Contacts from "@/components/pages/Contacts";
import AboutUs from "@/components/pages/AboutUs";

export default [
    {
        path: '/',
        redirect: '/users'
    },
    {
        path: '/users',
        name: 'users',
        component: Home
    },
    {
        path: '/users/:id',
        name: 'user',
        component: User
    },
    {
        path: '/contacts',
        name: 'contacts',
        component: Contacts
    },
    {
        path: '/about-us',
        name: 'about-us',
        component: AboutUs
    },
]
