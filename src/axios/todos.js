export const getTodosByUserId = (axios, userId, params, url = '/todos') =>
    axios.get(url, {params: {...params, userId}})
        .then(({data: todos}) => todos)
