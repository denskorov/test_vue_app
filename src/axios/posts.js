export const getPostsByUserId = (axios, userId, params, url = '/posts') =>
    axios.get(url, {params: {...params, userId}})
        .then(({data: posts}) => posts)
