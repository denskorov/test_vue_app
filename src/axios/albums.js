export const getAlbumsByUserId = (axios, userId, params, url = '/albums') =>
    axios.get(url, {params: {...params, userId}})
        .then(({data: albums}) => albums)
